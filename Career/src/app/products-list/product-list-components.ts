import { Component } from "@angular/core";

@Component({
    selector: "product-list",
    template: `
    
    <img class="card-img-top" height=50px src="http://www.pngmart.com/files/7/Cart-PNG-Transparent.png" alt="Card image cap">
    <div class="card" *ngFor="let product of products">
        <app-product [product]="product"></app-product>
     </div>


 
     
    `

})
export class ProductListComponent {
    products: any[] = [
        { id: 1, name: "Sony", version: 4.1, price: 3000, inStock: true, lastModifiedDate: Date.now() },
        { id: 2, name: "Samsung", version: 2.1, price: 4000, inStock: false, lastModifiedDate: Date.now() },
        { id: 3, name: "Apple", version: 9.1, price: 13000, inStock: true, lastModifiedDate: Date.now() }
    ];
    

}  