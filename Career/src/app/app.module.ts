import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserModule } from "@angular/platform-browser";
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ProductListComponent } from './products-list/product-list-components';
import { ProductComponent } from './product/product.component';
import { UserListComponent } from './user-list/userlist.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { UserComponent } from './user/user.component';


@NgModule({
    imports: [BrowserModule,HttpClientModule],
    declarations: [AppComponent,HomeComponent,HeaderComponent,FooterComponent,ProductListComponent,
        ProductComponent,UserListComponent,UserComponent],
    bootstrap: [AppComponent]
})
export class AppModule {

}
