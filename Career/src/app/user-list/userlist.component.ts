import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
    selector: "app-user-list",
    template: `
    
    <div *ngFor="let user of users">
        <app-user [user]="user"></app-user>
    </div>
    `,

    })
export class UserListComponent {
private users: any;
constructor(http: HttpClient) {
        http.get("https://api.github.com/users").subscribe(res=>{
            this.users=res; 
        });
    }
}