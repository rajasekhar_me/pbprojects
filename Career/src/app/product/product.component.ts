import { Component, Input } from '@angular/core';

@Component({
selector: "app-product", 
template:`
        
  <div class="card-body">
  <h1> {{product.name}} {{product.version}}</h1>
  <b> price:  {{product.price | currency : 'INR'}} </b>,
   in Stock: <input type ="checkbox" [checked]="product.inStock" disabled>
  
   <b> Last Modified date :: {{product.lastModifiedDate | date }}</b>
   
   <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
   <a href="#" class="btn btn-primary">Go somewhere</a>
   </div>
  

`
})
export class ProductComponent
{

    @Input()
    product: any;
    
}