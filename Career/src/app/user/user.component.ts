import { Input, Component } from '@angular/core';

@Component({
    selector: "app-user",
    styles: [`.circle-img{border-radius:150%}`],
    template: ` 
                <h1> {{user.login}}</h1>        
                <img class = "circle-img" [src]="user.avatar_url" alt="Image of avatar" height="70px" width="70px">
`

})
export class UserComponent {

    @Input()
    user: any;

}