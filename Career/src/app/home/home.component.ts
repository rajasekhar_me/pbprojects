import { Component } from '@angular/core';

@Component({
    selector: "home-comp",
    styles: [`.redClass{color:red} .greenClass{color:green}`],
    template: templateFunction()

})
export class HomeComponent {
    private counter = 0;
    private text: String;
    name: string = "rajasekhar.burepalli";
    private colorMine: String = "redClass";
    private isEnabled() {
        return false;
    }
    private inc() {
        this.counter++;
    }
    private dec() {
        this.counter--;
    }
    private onChangTextEvent(e) {
        this.text = e.target.value;
    }


}
function templateFunction(): string {
    return `<h1 class="redClass"> Home component</h1> <br> This is string interpolation {{name}} <br>
    <br>
    This is property binding: <input type="text" [value]="name">
    <br> <div> <input type="button" [disabled]="isEnabled()" value="isEnabled">  </div>
    
    <br> Event Type start <br>
    counter value: {{counter}}
    <br><input type="button" (click)="inc()" value ="Increment">
    <br><input type="button" (click)="dec()" value ="Decrement">
    <br> Event Type end <br>
    <br> class binding ::
    <h1 [class.redClass]="counter" [style.border]="counter? '3px solid red':0">{{counter}}</h1>

    <br> Directives :::
    <br> *ngIf:::    <h1 *ngIf="counter"> *ngIf directive {{counter}}</h1>
    <br>
    <br> *ngISwitch:::  <div [ngSwitch]="counter">
        <h1 *ngSwitchCase="1">High priority</h1>
        <h1 *ngSwitchCase="2"> Medium Priority</h1>
        <h1 *ngSwitchCase="3"> Low priority</h1>
        <h1 *ngSwitchDefault> Other priority </h1>
    </div>
    <br>
    onkeyup event
    <input type="text" (keyup)="onChangTextEvent($event)">
    <h1>{{text}}</h1>

     <br> Class directives
        <h1 ngClass="redClass greenClass">Hellooooo ngClass  way 1 using class  </h1>
        <h1 [ngClass]="colorMine">Hellooooo ngClass  way 2 using property (this property is class type)</h1>

    `;
}


